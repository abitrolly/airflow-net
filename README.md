Ansible deployment for Airflow cluster for on-premise data processing.

### Create test fleet with LXD

    lxc launch ubuntu:18.04 atc1
    lxc launch ubuntu:18.04 pilot1
    lxc launch ubuntu:18.04 pilot2
    lxc launch ubuntu:18.04 pilot3

    # forward port 8080 from controller container atc1 to host
    lxc config device add atc1 web0 proxy listen=tcp:127.0.0.1:8080 connect=tcp:127.0.0.1:8080

### Test connection with Ansible

    ansible -i inventory-lxd -c lxd atc1 -m setup

`-c lxd` tells to connect to inventory hosts using LXD.

```
atc1 | FAILED! => {
    "changed": false,
    "module_stderr": "/bin/sh: 1: /usr/bin/python: not found\n",
    "module_stdout": "",
    "msg": "MODULE FAILURE\nSee stdout/stderr for the exact error",
    "rc": 127
}
```

Test fails, because of missing Python, but connection is ok.

### Install Airflow and dependencies

    ansible-playbook -i inventory -c lxd playbook.yml

### Useful shortcuts for development

Speedup development by watching edited DAG file (`airflow-dag.py`) with `entr`
and uploading it to LXD on change.

    echo airflow-dag.py | entr -s "lxc file push airflow-dag.py \
     atc1/root/airflow/dags/ && lxc exec atc1 python airflow/dags/airflow-dag.py"
